package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/postregistro")
public class PostregistroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 @Override
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 doPost(request, response);
	 }
	 @Override
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
	 throws ServletException, IOException {
	 String id = request.getParameter("id");
	 String email = request.getParameter("email");
	 String password = request.getParameter("password");
	 /*
	 String action = registro ? "registro" : "login";
	 String enviar1 = registro ? "Registrar usuario" : "Iniciar sesi�n";
	 String enviar2 = registro ? "Registrando usuario..." : "Iniciando sesi�n...";
	 */
	 if (id != null && email != null && password != null) {
	 /* se reciben datos del formulario */
	 PrintWriter out = null;
	 try {
	 response.setCharacterEncoding("utf-8");
	 out = response.getWriter();
	 out.println("<!DOCTYPE html>");
	 out.println("<html>");
	 out.println("<head>");
	 out.println("<meta charset=\"UTF-8\" />");
	 out.println("<title>Post registro</title>");
	 out.print("<link rel=\"stylesheet\" type=\"text/css\" href=\"loginregistro.css\"");
	 out.println(" media=\"screen\" />");
	 out.println("<script type=\"text/javascript\" src=\"loginregistro.js\"></script>");
	 out.println("</head>");
	 out.println("<body onload=\"load()\">");
	 out.println("<div class=\"form\">");
	 /*
	 out.printf("<div class=\"error\"><p>%s</p></div>\n", mensaje);
	 out.printf("<form action=\"%s\" method=\"post\"", action);
	 out.printf(" onsubmit=\"return onSubmit('%s')\">\n", enviar2);
	 */
	 out.println("<p><label for=\"id\" id=\"idlbl\">Usuario</label></p>");
	 out.print("<p><input type=\"text\" name=\"id\" required=\"required\" pattern=\"\\w+\" ");
	 out.printf("value=\"%s\"", id);
	 out.println(" /></p>");
	 out.println("<p><label for=\"email\">eMail</label></p>");
	 out.print("<p><input type=\"email\" name=\"email\" required=\"required\" ");
	 out.print("pattern=\"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:");
	 out.println("[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\" /></p>");
	 out.println("<p><label for=\"password\">Contrase�a</label></p>\n");
	 out.print("<p><input type=\"password\" name=\"password\"");
	 out.println(" required=\"required\" /></p>\n");
	 out.print("<p class=\"bottom\"><input id=\"enviar\" type=\"submit\"");
	 /*
	 out.printf(" value=\"%s\" /></p>\n", enviar1);
	 */
	 out.println("</form>");
	 out.println("</div>");
	 out.println("</body>");
	 out.println("</html>");
	 /* escribe aqu� la sentencias que generan el contenido de la p�gina de postregistro */
	 } finally {
	 if (out != null)
	 out.close();
	 }
	 } else {
	 /* no se reciben datos de registro: se redirecciona a la p�gina de inicio */
	 response.sendRedirect("inicio");
	 }
	 }

}
