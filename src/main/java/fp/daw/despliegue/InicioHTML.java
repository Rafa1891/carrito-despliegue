package fp.daw.despliegue;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class InicioHTML {

	static public void enviar(HttpServletRequest request,HttpServletResponse response, Connection connection, HttpSession
			session)
			throws IOException {
			PrintWriter out = null;
			try {
				
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			/*
			* Escribe aqu� el c�digo que genere la p�gina de inicio siguiendo
			* las especificaciones siguientes:
			*
			* - Si el par�metro connection recibe el valor null, se ha producido
			* un error al intentar conectar con la base de datos. Muestra la
			* p�gina, pero en lugar del cat�logo de productos muestra un error.
			* En caso contrario utiliza el par�metro connection para leer los
			* productos de la base de datos.
			*
			* - Si el par�metro session es distinto de null, cada producto ir�
			* acompa�ado de un enlace "a�adir al carrito" que a�ada una unidad
			* del producto al carrito. Para generar la url del enlace:
			* request.getRequestURL() + "?id=" + id_de_producto"
			*
			* - El contenido del men� prinpipal var�a en funci�n de si el par�metro
			* session es null o no es null.
			*/
			
			/*
			 Connection connection = null;
			 Statement statement = null;
			 ResultSet resultSet = null;
			 String sql = null;
			 try {
			 connection = dataSource.getConnection();
			 statement = connection.createStatement();
			 sql = String.format("", id);
			 resultSet = statement.executeQuery(sql);
			 if (resultSet.next()) {
			 
			 } catch (SQLException | NoSuchElementException e) {
			 Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, e);
			 return "error interno, contacte con el administrador";
			 } finally {
			 if (resultSet != null)
			 try { resultSet.close(); } catch (SQLException e1) {}
			 if (statement != null)
			 try { statement.close(); } catch (SQLException e1) {}
			 if (connection != null)
			 try { connection.close(); } catch (SQLException e) {} }
			 }
			if(connection==null) {
				
			}else {
				connection.;
			}
			
			if(session!=null) {
				String url=request.getRequestURL();
				String id = request.getParameter("id");
				out.write("<a href="+url+"?id="+id+">A�adir al carrito</a>");
			}else {
				response.sendRedirect("inicio");
			}
			*/
			
			} finally {
			if (out != null)
			out.close();
			}
			}

}
